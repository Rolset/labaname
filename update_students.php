<?php 
require_once 'html/core.html';
require_once 'menu.php';
require_once 'connect.php';
if (isset($_GET['id'])) 
{
      $sth = $dbh->prepare("SELECT * FROM `students` WHERE `id` = ?");
	  $sth->execute(array($_GET["id"]));
      if($sth)
		{
   			while ($found = $sth->fetch(PDO::FETCH_BOTH))
   		    {
    			$name=$found['name'];
          $patronymic=$found['patronymic'];
          $surname=$found['surname'];
          $date=$found['date'];
          $files=$found['file'];
    		
             }
    	}
}
?>
<link rel="stylesheet" href="css/form2.css">
<script src="js/validation.js"></script>
<div class="form-wrap">
 <div class="profile"><img class="bd-placeholder-img rounded-circle" width="120" height="120" src="images/owl-47526_960_720.png" alt="">
    <h1>Информация о учениках</h1>
  </div>
 <form name="myForm" onSubmit="return formValidation();"  method="post" enctype="multipart/form-data">
     <div>
      <label >Фамилия</label>
      <input type="text" name="surname" value="<?= $surname?>">
    </div>
     <div>
      <label >Имя</label>
      <input type="text" name="name" value="<?= $name?>">
    </div>
    <div>
      <label >Отчество</label>
      <input type="text" name="patronymic" value="<?= $patronymic?>">
    </div>
    <div>
      <label >Дата рождения</label>
      <input type="text" name="date" value="<?= $date?>">
    </div> 
    <div>
      <label >Изображение</label>
       <?php echo "<img class='img-responsive' width='150' height='200' src='$files' alt=''>"; ?>
      <label >Изменить фото</label>
      <input type='file' name='avatar'>
    </div> 
    <button type="submit" name="submit" value="Submit">Сохранить изменения</button> 
  </form> 
  </div>
<?php

if(isset($_POST["submit"]))
{
$surname = $_POST["surname"];
$name = $_POST["name"];
$patronymic = $_POST["patronymic"];
$date = $_POST["date"];
$file = preg_replace("/[^A-Z0-9._-]/i", "_", $_FILES['avatar']['name']);
$file = 'uploads/' . time() . $file;
        if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $file))
         {
            $file = 'uploads/avatar-1577909_960_720.png';
        }
$sth = $dbh->prepare("UPDATE `students` SET `name` = :name, `patronymic` = :patronymic,`surname` = :surname, `date` = :date, `file` = :file WHERE `id` = :id");
$sth->execute(array('name' => $name, 'patronymic' => $patronymic, 'surname' => $surname, 'date' => $date,'file' => $file, 'id' => $_GET['id']));
header("Location: information_about_classes.php");
}
require_once 'html/foot.html';
require_once 'html/footer.html'; 
?>

