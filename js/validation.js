function formValidation()
{
var surname = document.myForm.surname;
var name = document.myForm.name;
var patronymic = document.myForm.patronymic;
var date = document.myForm.date;
if(filter_fullname(surname))
{
if(filter_class(name))
{
if(filter_schoolwork(patronymic))
{
if(filter_date(date))
{
}
else
return false;
}
else
return false;
}
else
return false;
}
else
return false;
}


function filter_fullname(surname)
{

var letters = /^[А-ЯЁ][а-яё]*$/;
var surname_len = surname.value.length;
if((surname.value.match(letters)))
{
	if ((surname_len > 50)||(surname_len < 2))
     {
        surname.style.border = "2px solid red";
		alert('Введено более 50 символов!');
		surname.focus();
		return false;
     }
     else
     {
surname.style.border = "2px solid green";
return true;
     }
}	
	else
		{
		surname.style.border = "2px solid red";
		alert('Пример: Иванов');
		surname.focus();
		return false;
		}
}

function filter_class(name)
{ 
	 
var letters = /^[А-ЯЁ][а-яё]*$/;
var name_len = name.value.length;
if((name.value.match(letters)))
{
	if ((name_len > 50)||(name_len < 2))
     {
        name.style.border = "2px solid red";
		alert('Введено более 50 символов!');
		name.focus();
		return false;
     }
     else
     {
name.style.border = "2px solid green";
return true;
     }
}	
	else
		{
		name.style.border = "2px solid red";
		alert('Пример: Иван');
		name.focus();
		return false;
		}
}

function filter_schoolwork(patronymic)
{

var letters = /^[А-ЯЁ][а-яё]*$/;
var patronymic_len = patronymic.value.length;
if((patronymic.value.match(letters)))
{
	if ((patronymic_len > 50)||(patronymic_len < 2))
     {
        patronymic.style.border = "2px solid red";
		alert('Введено более 50 символов!');
		patronymic.focus();
		return false;
     }
     else
     {
patronymic.style.border = "2px solid green";
return true;
     }
}	
	else
		{
		patronymic.style.border = "2px solid red";
		alert('Пример: Иванович');
		patronymic.focus();
		return false;
		}
}
function filter_date(date)
{ 
var letters = /^(200[3-9]|201[0-4]).(0[1-9]|1[012]).(0[1-9]|1[0-9]|2[0-9]|3[01])$/;
var date_len = date.value.length;
if((date.value.match(letters)))
{
date.style.border = "2px solid green";
return true;
}
else
{
date.style.border = "2px solid red";
alert('Пример: 2008.12.01');
date.focus();
return false;
}
}
