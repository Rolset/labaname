<?php 
require_once 'html/core.html';
require_once 'menu.php';
require_once 'connect.php';
if (isset($_GET['id'])) 
{
      $sth = $dbh->prepare("SELECT * FROM `classes` WHERE `id_class` = ?");
	  $sth->execute(array($_GET["id"]));
      if($sth)
		{
   			while ($found = $sth->fetch(PDO::FETCH_BOTH))
   		    {
    			$name=$found['teacher_name'];
    			$class=$found['class'];
    			$date=$found['graduation_date'];
    			$files=$found['teacher_avatar'];
    			$subject = $found['subject'];
    			$id_class=$found['id_class'];
    		
             }
    	}
}
?>
<link rel="stylesheet" href="css/form1.css">
<script src="js/validation2.js"></script>
<div class="form-wrap">
  <div class="profile"><img class="bd-placeholder-img rounded-circle" width="120" height="120" src="images/child-3326960_1280.png" alt="">
    <h1>Информация о классах</h1>
  </div>
 <form name="myForm" onSubmit="return formValidation();"  method="post" enctype="multipart/form-data">
     <div>
      <label >Номер класса</label>
      <input type="number" name="class"  value="<?= $class?>">
    </div>
     <div>
      <label >Классный руководитель</label>
      <input type="text" name="name" value="<?= $name?>">
    </div>
    <div>
      <label >Специальность учителя</label>
      <input type="text" name="schoolwork" value="<?= $subject?>">
    </div>
    <div>
      <label >Фото учителя</label>
      <?php echo "<img class='img-responsive' width='150' height='200' src='$files' alt=''>"; ?>
      <label >Изменить фото</label>
      <input type='file' name='avatar'>
    </div>
      <div>
      <label>Год выпуска</label>
      <input type="number" value="<?= $date?>" name="date" >
    </div>  
    <button type="submit" name="submit" value="Submit">Сохранить изменения</button> 
  </form> 
</div>
<?php

if(isset($_POST["submit"]))
{
$class = $_POST["class"];
$name = $_POST["name"];
$date = $_POST["date"];
$subject = $_POST["schoolwork"];
$file = preg_replace("/[^A-Z0-9._-]/i", "_", $_FILES['avatar']['name']);
$file = 'teacher/' . time() . $file;
        if (!move_uploaded_file($_FILES['avatar']['tmp_name'], $file))
         {
            $file = $files;
        }
$sth = $dbh->prepare("UPDATE `classes` SET `class` = :class, `teacher_name` = :teacher_name, `subject` = :subject, `teacher_avatar` = :teacher_avatar, `graduation_date` = :graduation_date WHERE `id_class` = :id_class");
$sth->execute(array('class' => $class,'teacher_name' => $name, 'subject' => $subject,'teacher_avatar' => $file,'graduation_date' => $date, 'id_class' => $_GET['id']));
header("Location: information_about_classes.php");
}
require_once 'html/foot.html';
require_once 'html/footer.html'; 
?>

