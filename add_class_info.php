<?php
require_once 'connect.php';
require_once 'html/core.html';
require_once 'menu.php';
?>
<link rel="stylesheet" href="css/form1.css">
<script src="js/validation2.js"></script>
<div class="form-wrap">
  <div class="profile"><img class="bd-placeholder-img rounded-circle" width="120" height="120" src="images/child-3326960_1280.png" alt="">
    <h1>Информация о классах</h1>
  </div>
 <form name="myForm" action="insert_class.php" onSubmit="return formValidation();"  method="post" enctype="multipart/form-data">
     <div>
      <label >Номер класса</label>
      <input type="number" name="class">
    </div>
     <div>
      <label >Классный руководитель</label>
      <input type="text" name="name">
    </div>
    <div>
      <label >Специальность учителя</label>
      <input type="text" name="schoolwork">
    </div>
    <div>
      <label >Фото учителя</label>
      <input type='file' name='avatar'>
    </div>
      <div>
      <label>Год выпуска</label>
      <input type="number" name="date">
    </div>  
    <button type="submit" name="submit" value="Submit">Добавить</button> 
  </form> 
</div>
<?php
require_once 'html/foot.html';
require_once 'html/footer.html'; 
?>
