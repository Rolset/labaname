<?php
global $x;
$x=15;
require_once 'connect.php';
require_once 'html/core.html';
require_once 'menu.php';
echo  '<a class="nav-link" href="add_class_info.php">Добавить информацию о классах</a>' ;
echo "<div class='row mb-2'>";
$i=1;  
$st = $dbh->prepare('SELECT * FROM classes');
$st->execute();
if($st)
{
   while ($found = $st->fetch(PDO::FETCH_BOTH))
   {
    $name=$found['teacher_name'];
    $class=$found['class'];
    $date=$found['graduation_date'];
    $file=$found['teacher_avatar'];
    $subject = $found['subject'];
    $id_class=$found['id_class'];
    echo "<div class='col-md-6'>";
      echo "<div class='row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative'>";
       echo "<div class='col-auto d-none d-lg-block'>";
         echo  "<div class='member-img'> <!-- Image  -->";
      echo "<img class='img-responsive' width='200' height='250' src='$file' alt=''>" . '</div>' . '</div>' ;
      echo  "<div class='col p-4 d-flex flex-column position-static'>";
        echo  "<strong class='d-inline-block mb-2 text-success'>Специальность: $subject</strong>";
         echo "<h3 class='mb-0'>$name</h3>";
         echo "<div class='mb-1 text-muted'>Классный руководитель $class-го класса</div>";
          echo "<p class='mb-auto'>Год выпуска класса: $date</p>";
          echo "<a class='nav-link' href='information_about_students.php?id=$id_class'>Просмотреть список учеников.</a></li>";
          echo "<a class='nav-link' href='update_class.php?id=$id_class'>Изменить</a></li>";
          echo "<a class='nav-link' href='delete_class.php?id=$id_class'><font  color='red'>Удалить</font></a></li>";
         echo "</div>" . "</div>" . "</div>";
  }
} 
echo "</div>";
require_once 'html/foot.html';
require_once 'html/footer.html'; 
?>